<?php

namespace backend\controllers;

use common\models\Post;
use common\models\search\PostSearch;
use Yii;
use yii\base\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Post models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Post();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                if ($model->imageFile) {
                    $model->uploadPhoto();
                }
                $model->user_id = \Yii::$app->user->id;
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing Services model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate(int $id): Response|string
    {
        $model = $this->findModel($id);
        if ($this->request->isPost) {
            if ($model->load($this->request->post() )) {
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                if ($model->image_file == null && $model->imageFile != null){
                    $model->uploadPhoto();
                }elseif ($model->imageFile != null){
                    $model->updatePhoto();
                }
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



/**
     * Increments the like count for a specific Post model.
     * @param int $id ID of the Post
     * @return array Response data
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionLike($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post = $this->findModel($id);

        if ($post) {
            $post->like_count += 1;
            $post->save(false);
            return ['success' => true, 'like_count' => $post->like_count];
        }

        return ['success' => false];
    }

    /**
     * Increments the dislike count for a specific Post model.
     * @param int $id ID of the Post
     * @return array Response data
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDislike($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post = $this->findModel($id);

        if ($post) {
            $post->dislike_count += 1;
            $post->save(false);
            return ['success' => true, 'dislike_count' => $post->dislike_count];
        }

        return ['success' => false];
    }
    public function actionIncrementView($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $post = Post::findOne($id);
        if ($post) {
            $post->views++;
            if ($post->save()) {
                return ['success' => true, 'views' => $post->views];
            }
        }
        return ['success' => false];
    }

}
