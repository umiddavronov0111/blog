<?php

namespace backend\controllers;

use Yii;
use yii\rest\Controller;
use common\models\ExchangeRate;

class ExchangeRateController extends Controller
{
    public function actionIndex()
    {
        return ExchangeRate::find()->all();
    }

    public function actionUpdateRates()
    {
        // Valyuta kurslarini yangilash logikasi
    }
}
