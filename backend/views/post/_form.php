
<?php

use common\models\Tag;
use kartik\editors\Codemirror;
use kartik\editors\Summernote;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap5\ActiveForm;

/** @var yii\web\View $this */
/** @var common\models\Post $model */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?=
    $form->field($model, 'tag_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Tag::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Tagni tanlang ...', 'multiple' => false],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [',', ' '],
            'maximumInputLength' => 10
        ],
    ])->label('Tag Multiple');
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description', [
        'labelOptions' => ['label' => ' Short Description ']
    ])->widget(Summernote::class, [
        'useKrajeePresets' => true,
        'pluginOptions' =>  [
            'height' => 300,
            'dialogsFade' => true,
            'toolbar' => [
                ['style1', ['style']],
                ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
                ['font', ['fontname', 'fontsize', 'color', 'clear']],
                ['para', ['ul', 'ol', 'paragraph', 'height']],
                ['insert', ['link', 'picture', 'video', 'table', 'hr']],
            ],
            'fontSizes' => ['8', '9', '10', '11', '12', '13', '14', '16', '18', '20', '24', '36', '48'],
            'codemirror' => [
                'theme' => Codemirror::DEFAULT_THEME,
                'lineNumbers' => true,
                'styleActiveLine' => true,
                'matchBrackets' => true,
                'smartIndent' => true,
            ],
        ]
    ]); ?>

    <br>
    <?= $form->field($model, 'imageFile')->label('Image')->fileInput([ 'accept' => 'image/png/jpeg']) ?>
    <br>





    <?=  $form->field($model, 'big_description',
    [ 'labelOptions' => ['label' => ' Big Description ']
    ])->widget(Summernote::class, [
        'useKrajeePresets' => true,
        'pluginOptions' =>  [
            'height' => 300,
            'dialogsFade' => true,
            'toolbar' => [
                ['style1', ['style']],
                ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
                ['font', ['font name', 'fontsize', 'color', 'clear']],
                ['para', ['ul', 'ol', 'paragraph', 'height']],
                ['insert', ['link', 'picture', 'video', 'table', 'hr']],
            ],
            'fontSizes' => ['8', '9', '10', '11', '12', '13', '14', '16', '18', '20', '24', '36', '48'],
            'codemirror' => [
                'theme' => Codemirror::DEFAULT_THEME,
                'lineNumbers' => true,
                'styleActiveLine' => true,
                'matchBrackets' => true,
                'smartIndent' => true,
            ],
        ]
    ]); ?>


    <?= $form->field($model, 'status')->dropDownList([1=>'active', 0=>'inactive']) ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


