<?php

use common\models\helpers\StatusHelpers;
use common\models\Post;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var common\models\search\PostSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],



            [
                'attribute' => 'user_id',
                'label'=>'Username',
                'value' => function ($model) {
                    return  $model->user->username;
                }
            ],

            [
                'attribute' => 'tag_id',
                'label'=>'Tag',
                'value' => function ($model) {
                    return  $model->tag->name;
                }
            ],

            'title',

            [
                'attribute' => 'image_file',
                'value' => function ($data) {
                    return $data->getPhotoSrc();
                },
                'format' => ['image', ['width' => 140, 'height' => 80]],
            ],
            [
                'attribute' => 'description',
                'value' => function ($data) {
                    return strip_tags($data->description);
                },
                'format' => 'text',
            ],
              'created_at:date',
            [
                'attribute' => 'big_description',
                'value' => function ($data) {
                    return strip_tags($data->big_description);
                },
                'format' => 'text',
            ],

            [
                'attribute' => 'status',
                'value'=> function (Post $model) {
                    return StatusHelpers::getStatusLabel($model->status);
                },
                'format' => 'html',
                'filter' => StatusHelpers::getStatusList()

            ],

            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Post $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
