<?php

use common\models\helpers\StatusHelpers;
use common\models\Post;
use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var common\models\Post $model */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'attribute' => 'image_file',
                'label'=>'Image',
                'value' => function ($data) {
                    return $data->getPhotoSrc();
                },
                'format' => ['image', ['width' => 140, 'height' => 80]]
            ],
            'title',
            [
                'attribute' => 'description',
                'value' => function ($data) {
                    return strip_tags($data->description);
                },
                'format' => 'text',
            ],
            [
                'attribute' => 'big_description',
                'value' => function ($data) {
                    return strip_tags($data->big_description);
                },
                'format' => 'text',
            ],

//            [
//                'format' => 'html',
//                'attribute' => 'user_id',
//                'value' => ($model->user ?
//                    Html::a('<i class="glyphicon glyphicon-list"></i>', ['user/index']).' '.
//                    Html::a('<i class="glyphicon glyphicon-circle-arrow-right"></i> '.$model->user->name, ['user/view', 'id' => $model->user->id,]).' '.
//                    Html::a('<i class="glyphicon glyphicon-paperclip"></i>', ['create', 'Chap'=>['user_id' => $model->name]])
//                    :
//                    '<span class="label label-warning">?</span>'),
//            ],
            [
                'format' => 'html',
                'attribute' => 'tag_id',
                'value' => ($model->tag ?
                    Html::a('<i class="glyphicon glyphicon-list"></i>', ['tag/index']).' '.
                    Html::a('<i class="glyphicon glyphicon-circle-arrow-right"></i> '.$model->tag->name, ['tag/view', 'id' => $model->tag->id,]).' '.
                    Html::a('<i class="glyphicon glyphicon-paperclip"></i>', ['create', 'Chap'=>['tag_id' => $model->tag_id]])
                    :
                    '<span class="label label-warning">?</span>'),
            ],
            [
                'attribute' => 'status',
                'value'=> function (Post $model) {
                    return StatusHelpers::getStatusLabel($model->status);
                },
                'format' => 'html',


            ],


        ],
    ]) ?>

</div>
