<?php

use yii\db\Migration;

/**
 * Class m240502_075422_add_user_colum_role
 */
class m240502_075422_add_user_colum_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user','role', $this->string(30)->after('email'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m240502_075422_add_user_colum_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240502_075422_add_user_colum_role cannot be reverted.\n";

        return false;
    }
    */
}
