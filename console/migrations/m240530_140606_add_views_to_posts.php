<?php

use yii\db\Migration;

/**
 * Class m240530_140606_add_views_to_posts
 */
class m240530_140606_add_views_to_posts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%post}}', 'views', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%post}}', 'views');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240530_140606_add_views_to_posts cannot be reverted.\n";

        return false;
    }
    */
}
