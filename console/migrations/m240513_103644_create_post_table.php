<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%post}}`.
 */
class m240513_103644_create_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if (Yii::$app->db->getTableSchema('post', true) != null) {
            $this->dropTable('post');
        }

        $this->createTable('post', [
            'id' => $this->primaryKey(),

            'title' => $this->string()->notNull(),
            'image_file' => $this->string()->notNull(),
            'description'=>$this->text()->notNull(),
            'user_id' => $this->integer(20)->notNull(),
            'tag_id' => $this->integer()->notnull(),
            'big_description'=>$this->text()->notNull(),
            'like_count'=> $this->integer()->notNull()->defaultValue(0),
            'dislike_count'=> $this->integer()->notNull()->defaultValue(0),




            'status' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'is_deleted' => $this->tinyInteger(1)->notNull()->defaultValue(0),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            'post_and_user_frk',
            'post',
            'user_id',
            'user',
            'id'
        );
        $this->addForeignKey(
            'post_tag_fk',
            'post',
            'tag_id',
            'tag',
            'id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post');
    }
}
