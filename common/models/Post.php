<?php

namespace common\models;


use common\models\helpers\traits\TraitUploadFile;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $title
 * @property string $image_file
 * @property string|null $description
 * @property string|null $big_description
 * @property int $user_id
 * @property int $tag_id
 * @property int $status
 * @property int $is_deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $views
 *
 * @property Tag $tag
 * @property User $user
 */
class Post extends \yii\db\ActiveRecord
{

    use  TraitUploadFile;

    const PATH_PHOTO = '/uploads/photos/post';
    const PATH_FILE = 'post_';

    public $imageFile;
    /**
     * @var mixed|null
     */
    public $like_count = 0;

    public $dislike_count = 0;


    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            BlameableBehavior::class,

        ];
    }
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'image_file', 'user_id', 'tag_id','big_description', ], 'required'],
            [['user_id', 'tag_id', 'status', 'is_deleted', 'created_at', 'updated_at', 'created_by', 'updated_by','views'], 'integer'],
            [['title', 'image_file', 'description'], 'string', 'max' => 255],
            [['created_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tag::class, 'targetAttribute' => ['tag_id' => 'id']],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image_file' => 'Image File',
            'description' => 'Description',
            'big_description' => 'Big Description',
            'user_id' => 'User ID',
            'tag_id' => 'Tag ID',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[Tag]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tag::class, ['id' => 'tag_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
