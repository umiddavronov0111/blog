<?php

namespace common\models;

use common\models\helpers\traits\TraitUploadFile;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property string $image_file
 * @property string $image_file1
 * @property string $image_file2
 * @property int $status
 * @property int $is_deleted
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Image extends \yii\db\ActiveRecord
{

    use  TraitUploadFile;

    const PATH_PHOTO = '/uploads/photos/image';
    const PATH_FILE = 'image_';

    public $imageFile;
    public $imageFile1;
    public $imageFile2;


    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            BlameableBehavior::class,

        ];
    }
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image_file', 'image_file1', 'image_file2', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['status', 'is_deleted', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['image_file', 'image_file1', 'image_file2'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            [['imageFile1'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            [['imageFile2'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_file' => 'Image File',
            'image_file1' => 'Image File1',
            'image_file2' => 'Image File2',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
