<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class ExchangeRate extends ActiveRecord
{
    public static function tableName()
    {
        return 'exchange_rate';
    }

    public function rules()
    {
        return [
            [['currency', 'rate'], 'required'],
            [['rate'], 'number'],
            [['currency'], 'string', 'max' => 3],
        ];
    }

    public function attributeLabels()
    {
        return [
            'currency' => 'Currency',
            'rate' => 'Rate',
        ];
    }
}
