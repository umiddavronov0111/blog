<?php

namespace common\models;

use common\models\helpers\traits\TraitUploadFile;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $big_description
 * @property int $status
 * @property int $is_deleted
 * @property string $update_at
 * @property string $create_at
 * @property int $created_by
 * @property int $updated_by
 */
class Categories extends \yii\db\ActiveRecord
{
    use  TraitUploadFile;


    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            BlameableBehavior::class,

        ];
    }


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'big_description', 'created_by', 'updated_by'], 'required'],
            [['status', 'is_deleted', 'created_by', 'updated_by'], 'integer'],
            [['update_at', 'create_at'], 'safe'],
            [['name', 'description', 'big_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'big_description' => 'Big Description',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'update_at' => 'Update At',
            'create_at' => 'Create At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
