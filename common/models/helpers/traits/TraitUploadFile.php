<?php

namespace common\models\helpers\traits;

use Yii;
use yii\helpers\Url;

trait TraitUploadFile
{
    public static function getLastId(): int
    {
        $lastId = self::find()->select('id')->orderBy(['id' => SORT_DESC])->scalar() ?? 0;
        return ++$lastId;
    }

    public static function getPhotoAlias(): string
    {
        return Yii::getAlias('@appRoot') . self::PATH_PHOTO;
    }


    public function getPhotoSrc(): string
    {
        return Url::to(self::PATH_PHOTO . '/' . $this->image_file);
    }

    public function isPhotoExists(): bool
    {
        return file_exists(self::getPhotoAlias() . '/' . $this->image_file);
    }


    public function deletePhoto(): bool
    {
        return unlink(self::getPhotoAlias() . '/' . $this->image_file);
    }


    public function generatePhotoName(): string
    {
        return self::PATH_FILE . self::getLastId() . '-' . (int)(microtime(true) * (1000)) . '.' . $this->imageFile->extension;
    }


    public function getSavePhoto(string $photoName): void
    {
        $this->imageFile->saveAs(self::getPhotoAlias() . '/' . $photoName);
    }


    public function fileCreated(): void
    {
        if (!file_exists(self::getPhotoAlias())) {
            mkdir(self::getPhotoAlias(), 0777, true);
        }
    }


    public function imageDb(string $photoName): void
    {
        $this->image_file = $photoName;
    }


    public function updatePhoto(): void
    {
        if ($this->validate()){
            if ($this->isPhotoExists()) {
                $this->deletePhoto();
            }
            $this->uploadPhoto();
        }
    }

    public function uploadPhoto(): bool
    {
        if ($this->validate(false)){
            $photoName = $this->generatePhotoName();
            $this->fileCreated();
            $this->getSavePhoto($photoName);
            $this->imageDb($photoName);
            return true;
        }
        return false;
    }


    public function deleteUpdatePhoto($image): bool
    {
        return unlink(self::getPhotoAlias() . '/' . $image);
    }

}
