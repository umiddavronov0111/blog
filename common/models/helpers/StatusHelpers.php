<?php

namespace common\models\helpers;


use common\models\Post;
use Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class StatusHelpers
{
    public static function  getStatusList(): array
    {
        return [
            Post::STATUS_ACTIVE => 'active',
            Post::STATUS_INACTIVE => 'inactive',
        ];
    }

    /**
     * @throws Exception
     */
    public static function getStatusName($status)
    {
        return ArrayHelper::getValue(self::getStatusList(), $status);
    }

    /**
     * @throws Exception
     */
    public static function getStatusLabel($status): string
    {
        switch ($status) {
            case Post::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            case Post::STATUS_INACTIVE:
                $class = 'label label-danger';
                break;
            default:
                $class = 'label label-default';
        }
        return Html::tag('span', ArrayHelper::getValue(self::getStatusList(), $status), [
            'class' => $class,
        ]);
    }
}