<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'frontend/web/css/main.css',
        'frontend/web/css/vendor.css',
    ];
    public $js = [
        'js/script/app.js',
        'js/script/jquery.min.js',
        'https://www.googletagmanager.com/gtag/js?id=G-MP1XPBWZMR'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap5\BootstrapAsset',
    ];
}
