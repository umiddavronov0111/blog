<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $exception \yii\web\HttpException */

$this->title = 'Sahifa topilmadi';
?>

<style>
    body {
        background: url('/path/to/your/image.jpg') no-repeat center center fixed;
        background-size: cover;
        color: #fff;
        font-family: Arial, sans-serif;
        text-align: center;
        padding: 10% 0;
    }

    .error-container {
        background: rgba(0, 0, 0, 0.6);
        padding: 20px;
        border-radius: 10px;
        display: inline-block;
        max-width: 600px;
        margin: auto;
    }

    .error-container h1 {
        font-size: 100px;
        margin: 0;
    }

    .error-container p {
        font-size: 20px;
    }

    .error-container a {
        color: #fff;
        text-decoration: underline;
    }
</style>

<div class="error-container">
    <h1>404</h1>

    <p>
        <?= nl2br(Html::encode($exception->getMessage())) ?>
    </p>
    <p>
        <a href="<?= Yii::$app->homeUrl ?>">Bosh sahifaga qaytish</a>
    </p>
</div>
