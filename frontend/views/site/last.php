<?php
use common\models\Tag;
use yii\bootstrap5\Html;
use yii\helpers\Url;

?>
<section id="article-list">
    <?php foreach ($latestPosts as $model): ?>
        <a href="<?= Url::to(['/site/detail', 'id' => $model->id]) ?>" class="news-card sec-padding ar-item">
            <div class="sec-padding">
                <div class="m-title">Tavsiya etamiz</div>
                <a href="/"
                   class="fast-news sec-mini">
                    <div class="fast-news__img"><img
                                src="https://fivm.giglink.uz/source/thumbnails/_md/1/A3Meu2vB9e2ExaF8RTMWOfxpheW6_FKM_md.jpg">
                    </div>
                    <div class="fast-news__info">Apple yangi katta yangilikka muhtoj: Tim Kukda gʻoyalar
                        tugab qolgan
                    </div>
                </a>

            </div>
        </a>
    <?php endforeach; ?>
</section>
