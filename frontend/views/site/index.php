<?php

use yii\widgets\ListView;

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-content">

    <div class="site-wrapper">
        <div class="container">

            <div class="sec-padding">
                <?= $this->render('about') ?>








                <div class="col-12 col-sm-12  col-xl-9">

    <?= ListView::widget([
        'dataProvider' => $dataProvider,

        'options' => ['class' => 'row'],
        'itemOptions' => ['class' => 'col-lg-12'],
        'layout' => "{items}\n{pager}",
        'itemView' => function ($model, $key, $index, $widget) {
            return $this->render('_post', ['model' => $model]);
        },
        'pager' => [
            'class' => 'yii\bootstrap5\LinkPager',
        ],
    ]); ?>


                    </div>
                </div>


            </div>
        </div>
    </div>



