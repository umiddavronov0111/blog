<?php
use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model frontend\models\Post */
/* @var $rates array */

$this->title = 'Exchange Rates';
$this->params['breadcrumbs'][] = $this->title;
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    function getExchangeRates() {
        $.ajax({
            url: '/site/exchange-rates',
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                if (data && data.rates) {
                    var rates = data.rates;
                    var html = '';
                    for (var i = 0; i < rates.length; i++) {
                        html += '<tr>';
                        html += '<td>' + rates[i].title + '</td>';
                        html += '<td>' + rates[i].cb_price + '</td>';
                        html += '</tr>';
                    }
                    $('#exchange-rates-table tbody').html(html);
                } else {
                    console.log('Exchange rates not available.');
                }
            },
            error: function(xhr, status, error) {
                console.error('Error:', error);
            }
        });
    }

    $(document).ready(function() {
        getExchangeRates();
    });
</script>

<div class="site-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <table id="exchange-rates-table" class="table">
        <thead>
        <tr>
            <th scope="col">Currency</th>
            <th scope="col">Rate</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

<!--    <h2>Posts</h2>-->
<!--    --><?php //= ListView::widget([
//        'dataProvider' => $dataProvider,
//        'itemView' => '_postItem',
//    ]); ?>
</div>
