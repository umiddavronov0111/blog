 <div class="row">
    <div class="col-xl-9">
        <?= $this->render('exchange-rates' ) ?>
    </div>
    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-3">
            <div class="sec-padding">
                <div class="m-title">Tavsiya etamiz</div>
                <a href="/"
                   class="fast-news sec-mini">
                    <div class="fast-news__img"><img
                                src="https://fivm.giglink.uz/source/thumbnails/_md/1/A3Meu2vB9e2ExaF8RTMWOfxpheW6_FKM_md.jpg">
                    </div>
                    <div class="fast-news__info">Apple yangi katta yangilikka muhtoj: Tim Kukda gʻoyalar
                        tugab qolgan
                    </div>
                </a>

            </div>
        <div class="sticky-el sec-padding">
            <div class="help-section">
                <ul class="help-section__list">
                    <li><a href="/" class="help-section__link">Loyiha haqida</a></li>
                </ul>
                <div class="help-section__description">© 2023 giglink.uz</div>
            </div>
            <ul class="social-mini">
                <li><a href="https://www.facebook.com" target="_blank"
                       class="social-mini__link facebook">
                        <svg width="12" height="23" viewBox="0 0 12 23" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M11.5481 0.555275L8.66986 0.550781C5.43624 0.550781 3.34653 2.63723 3.34653 5.86657V8.3175H0.452578C0.202505 8.3175 0 8.5148 0 8.75817V12.3093C0 12.5527 0.202736 12.7497 0.452578 12.7497H3.34653V21.7103C3.34653 21.9537 3.54904 22.1508 3.79911 22.1508H7.5749C7.82497 22.1508 8.02748 21.9535 8.02748 21.7103V12.7497H11.4112C11.6613 12.7497 11.8638 12.5527 11.8638 12.3093L11.8652 8.75817C11.8652 8.64131 11.8174 8.52941 11.7326 8.44671C11.6479 8.36402 11.5324 8.3175 11.4123 8.3175H8.02748V6.23982C8.02748 5.2412 8.27201 4.73425 9.60873 4.73425L11.5477 4.73357C11.7975 4.73357 12 4.53627 12 4.29314V0.995711C12 0.752797 11.7977 0.555725 11.5481 0.555275Z"
                                  fill="#bababa"/>
                        </svg>
                    </a></li>
                <li><a href="https://t.me" target="_blank"
                       class="social-mini__link telegram">
                        <svg width="24" height="21" viewBox="0 0 24 21" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M22.359 0.479006C20.7653 1.06494 1.10121 8.6165 0.810583 8.75713C-0.150355 9.21182 -0.262855 9.81182 0.538708 10.2196C0.618395 10.2618 1.94496 10.6837 3.49652 11.1665L6.31371 12.0431L12.7075 8.03994C16.2278 5.8415 19.1762 4.01338 19.2606 3.98525C19.4809 3.91025 19.6684 3.91494 19.7528 4.004C19.8137 4.06494 19.8184 4.09307 19.7668 4.18213C19.7387 4.24307 17.3996 6.3665 14.5778 8.89775C11.7559 11.4337 9.43558 13.5243 9.42621 13.5478C9.41683 13.5712 9.32308 14.8368 9.21527 16.3603L9.0184 19.1259L9.23402 19.0978C9.35121 19.0837 9.53402 19.0228 9.64183 18.9618C9.75433 18.9056 10.4856 18.2399 11.2778 17.4853C12.0653 16.7306 12.7403 16.0978 12.7731 16.0884C12.8059 16.0743 14.0621 16.9649 15.5668 18.0665C17.0668 19.1681 18.4121 20.1196 18.5575 20.1853C18.9371 20.3634 19.359 20.3774 19.6356 20.2321C19.884 20.1009 20.1231 19.7728 20.2262 19.4306C20.2637 19.2993 21.1262 15.329 22.134 10.6087C23.8168 2.75244 23.9715 1.99307 23.9715 1.58525C23.9715 1.20088 23.9575 1.10713 23.8543 0.905567C23.784 0.760256 23.6575 0.614943 23.5356 0.530567C23.259 0.338381 22.7996 0.314943 22.359 0.479006Z"
                                  fill="#BABABA"/>
                        </svg>
                    </a></li>
                <li><a href="https://twitter.com    " target="_blank"
                       class="social-mini__link twitter">
                        <svg xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="256"
                             height="256" viewBox="0 0 256 256" xml:space="preserve">
                                                <g style="stroke:none;stroke-width:0;stroke-dasharray:none;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;fill:none;fill-rule:nonzero;opacity:1"
                                                   transform="translate(1.4065934065934016 1.4065934065934016) scale(2.81 2.81)">
                                                    <path d="M 28.303 81.565 c 33.962 0 52.538 -28.138 52.538 -52.538 c 0 -0.799 0 -1.595 -0.054 -2.387 c 3.614 -2.614 6.733 -5.85 9.212 -9.558 c -3.37 1.493 -6.945 2.473 -10.606 2.905 c 3.855 -2.308 6.74 -5.937 8.118 -10.213 c -3.625 2.151 -7.59 3.667 -11.725 4.482 c -6.993 -7.436 -18.69 -7.795 -26.126 -0.802 c -4.796 4.51 -6.83 11.23 -5.342 17.643 C 29.473 30.352 15.64 23.34 6.264 11.804 c -4.901 8.437 -2.398 19.231 5.717 24.649 c -2.939 -0.087 -5.813 -0.88 -8.381 -2.311 c 0 0.076 0 0.155 0 0.234 c 0.002 8.79 6.198 16.36 14.814 18.101 c -2.718 0.741 -5.571 0.85 -8.338 0.317 c 2.419 7.522 9.351 12.675 17.251 12.823 c -6.539 5.139 -14.616 7.928 -22.932 7.92 C 2.926 73.534 1.459 73.445 0 73.27 c 8.444 5.419 18.27 8.293 28.303 8.28"
                                                          style="stroke:none;stroke-width:1;stroke-dasharray:none;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;fill-rule:nonzero;opacity:1"
                                                          transform=" matrix(1 0 0 1 0 0) " stroke-linecap="round"
                                                          fill="#BABABA"/>
                                                </g>
                                            </svg>
                    </a></li>
            </ul>
        </div>
    </div>
</div>

