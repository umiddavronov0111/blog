<?php
// frontend/views/site/search.php

/** @var yii\web\View $this */
/** @var string $query */
/** @var array $results */

use common\models\Tag;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Qidirilgan  : ' . Html::encode($query);
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-search">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (empty($results)): ?>
        <p>Bunga tegishli hechnima topilmadi : <strong><?= Html::encode($query) ?></strong>.</p>
    <?php else: ?>
        <ul>
            <?php foreach ($results as $result): ?>
                <li>


                    <div class="page-content">

                        <div class="site-wrapper">
                            <div class="container">
                                <div class="sec-padding">

                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12 col-xl-9">
                                            <section id="article-list">
                                                <a href="<?= Url::to(['/site/detail', 'id' => $result->id]) ?>" class="news-card sec-padding ar-item">
                                                    <div class="news-card__img">
                                                        <img src="<?= $result->getPhotoSrc() ?>" alt="/"/>
                                                    </div>

                                                    <div class="news-card__info">
                                                        <div class="m-title mb-15"><?= Html::encode($result->title) ?></div>
                                                        <div class="m-text mb-15"><?= Html::decode($result->description) ?></div>
                                                        <div class="news-card__bottom">
                                                            <div class="news-card__type wl-ct-name">
                                                                <?php
                                                                $tag = Tag::findOne($result->tag_id);
                                                                if ($tag) {
                                                                    echo Html::encode($tag->name);
                                                                } else {
                                                                    echo 'Noma\'lum Tag';
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="news-card__date">
                                                                <img src="https://cdn0.iconfinder.com/data/icons/set-ui-app-android/32/9-512.png"/>
                                                                <span> <?= Yii::$app->formatter->asRelativeTime($result->updated_at) ?></span>
                                                            </div>
                                                        </div>


                                                    </div>

                                                </a>
                                            </section>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>

