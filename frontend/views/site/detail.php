<?php

use common\models\Tag;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="page-content">
    <div class="site-wrapper">
        <div class="container">
            <div class="sec-padding">
                <div class="row">
                    <!-- Main content column -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9">
                        <section class="sec-padding description-section">
                            <h1 class="m-title-big mb-30"><?= Html::encode($model->title) ?></h1>
                            <div class="description-section__info sec-padding">
                                <div class="news-card__type wl-ct-name">
                                    <?php
                                    $tag = Tag::findOne($model->tag_id);
                                    if ($tag) {
                                        echo Html::encode($tag->name);
                                    } else {
                                        echo ' Tag nomi topilmadi ';
                                    }
                                    ?>
                                </div>
                                <div class="description-section__box">
                                    <img src="https://uxwing.com/wp-content/themes/uxwing/download/time-and-date/clock-icon.png">
                                    <span><?= Yii::$app->formatter->asRelativeTime($model->updated_at) ?></span>
                                </div>
                            </div>
                            <h3 class="m-title"><?= Html::decode($model->description) ?></h3>
                            <div class="rich-text-box content px-2">
                                <div class="content-img">
                                    <figure class="jj paragraph-image">
                                        <div class="eq er agb">
                                            <picture class="image">
                                                <img src="<?= $model->getPhotoSrc() ?>">
                                            </picture>
                                        </div>
                                        <figcaption class="ju dw"></figcaption>
                                    </figure>
                                </div>
                                <div class="is-block-pr">
                                    <p><?= Html::decode($model->big_description) ?></p>
                                </div>
                                <div class="is-block-pr">
                                    <p></p>
                                </div>
                                <div class="content-img">
                                    <figure class="jj paragraph-image">
                                        <div class="eq er agb">
                                            <picture class="image" data-src="https://fivm.giglink.uz/source/1/TLPbyDB5fQ6geM7gZNceqYeDSfPRRnXO.png">
                                            </picture>
                                        </div>
                                        <figcaption class="ju dw">© News</figcaption>
                                    </figure>
                                </div>
                            </div>
                            <section class="sec-padding"></section>
                            <div class="social-section">
                                <div class="social-section__title">Bizning Telegram kanalimizga obuna boʻling</div>
                                <a href="https://t.me/thenew_s" target="_blank" class="social-section__button">
                                    <img width="30" height="30" src="https://img.icons8.com/ios-filled/50/telegram-app.png" alt="telegram-app"/>
                                    <span></span>
                                </a>
                            </div>
                        </section>
                    </div>







    <!--                    <section class="sec-padding">-->
    <!--                        <h2 class="m-title-big mb-30">Soʻnggi yangiliklar</h2>-->
    <!---->
    <!--                        <a href="/"-->
    <!--                           class="news-card sec-padding ar-item">-->
    <!--                            <div class="news-card__img"><img-->
    <!--                                        src="--><?php //= $model->getPhotoSrc()?><!--"/>-->
    <!--                            </div>-->
    <!--                            <div class="news-card__info">-->
    <!--                                <div class="m-title mb-15">--><?php //= Html::decode($model->title) ?>
    <!--                                </div>-->
    <!--                                <div class="m-text mb-15">--><?php //= Html::decode($model->description) ?>
    <!--                                </div>-->
    <!--                                <div class="news-card__bottom">-->
    <!--                                    <div class="news-card__type wl-ct-name"> --><?php
    //                                        $tag = Tag::findOne($model->tag_id);
    //                                        if ($tag) {
    //                                            echo Html::encode($tag->name);
    //                                        } else {
    //                                            echo ' Tag nomi topilmadi ';
    //                                        }
    //                                        ?><!--</div>-->
    <!--                                    <div class="news-card__date"><img-->
    <!--                                                src="/assets/a6f2ff8d/images/icon/clock.svg"/>-->
    <!--                                        <span>--><?php //= Yii::$app->formatter->asRelativeTime($model->updated_at) ?><!--</span>-->
    <!--    -->
    <!--                                    </div>-->
    <!--                                </div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </section>-->

                    <!-- About section column -->
<!--                    <div class="col-12 col-sm-12 col-md-12 col-lg-3">-->
<!--                        <section id="article-list" class="sec-padding">-->
<!--                            <h2>Oxirgi yangiliklar</h2>-->
<!--                            --><?php //foreach ($latestPosts as $post): ?>
<!--                                <a href="--><?php //= Url::to(['/site/detail', 'id' => $post->id]) ?><!--" class="news-card sec-padding ar-item">-->
<!--                                    <div class="sec-padding">-->
<!--                                        <div class="m-title">--><?php //= Html::decode($model->title) ?><!--</div>-->
<!--                                        <a href="/"-->
<!--                                           class="fast-news sec-mini">-->
<!--                                            <div class="fast-news__img"><img-->
<!--                                                        src="--><?php //= $model->getPhotoSrc() ?><!--">-->
<!--                                            </div>-->
<!--                                            <div class="fast-news__info">--><?php //= Html::decode($model->description) ?>
<!--                                            </div>-->
<!--                                        </a>-->
<!---->
<!--                                    </div>-->
<!--                                </a>-->
<!--                            --><?php //endforeach; ?>
<!--                        </section>-->
<!--                    </div>-->

                </div>
            </div>
        </div>
    </div>
</div>
