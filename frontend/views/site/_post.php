<?php
/* @var $model common\models\Post */

use common\models\Tag;
use yii\bootstrap5\Html;
use yii\helpers\Url;

$tags = Tag::listTags();
?>

<section id="article-list">
    <a href="<?= Url::to(['/site/detail', 'id' => $model->id]) ?>" class="news-card sec-padding ar-item">
        <div class="news-card__img">
            <img src="<?= $model->getPhotoSrc() ?>" alt="Image"/>
        </div>

        <div class="news-card__info">
            <div class="m-title mb-15"><?= Html::encode($model->title) ?></div>
            <div class="m-text mb-15"><?= Html::decode($model->description) ?></div>
            <div class="news-card__bottom">
                <div class="news-card__type wl-ct-name">
                    <?php
                    $tag = Tag::findOne($model->tag_id);
                    if ($tag) {
                        echo Html::encode($tag->name);
                    } else {
                        echo 'Unknown Tag';
                    }
                    ?>
                </div>
                <div class="news-card__date">
                    <img src="https://cdn0.iconfinder.com/data/icons/set-ui-app-android/32/9-512.png" alt="Clock Icon"/>
                    <span><?= Yii::$app->formatter->asRelativeTime($model->updated_at) ?></span>
                </div>
            </div>
            <div class="post-view">
                <p id="view-count">Ko'rilganlar soni: <?= $model->views ?></p>
            </div>
        </div>
    </a>
</section>

<?php
$incrementViewUrl = Url::to(['post/increment-view', 'id' => $model->id]);

$this->registerJs(<<<JS
setTimeout(function() {
    $.ajax({
        url: '$incrementViewUrl',
        method: 'POST',
        success: function(data) {
            if (data.success) {
                $('#view-count').text('Ko\'rilganlar soni: ' + data.views);
            }
        }
    });
}, 10000); // 10 seconds (10000 milliseconds) delay
JS);
?>
