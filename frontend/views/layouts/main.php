<?php

/** @var \yii\web\View $this */
/** @var string $content */

use common\models\Tag;
use frontend\assets\AppAsset;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;

AppAsset::register($this);
$tags = Tag::listTags();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>
<header>

    <?php
    NavBar::begin([

        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Bosh sahifa', 'url' => ['/site/index']],
    ];

    foreach ($tags as $key => $value) {
        $menuItems[] = [
            'label' => $value,
            'url' => ['/site/index', 'tag_id' => $key]
        ];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav me-auto mb-4 mb-md-0'],
        'items' => $menuItems,
    ]);

    // Qidiruv paneli
    echo '<form class="form-inline my-2 my-lg-0" action="'. \yii\helpers\Url::to(['site/search']) .'" method="get">';
    echo '<input class="form-control mr-sm-2" type="search" placeholder="Qidiruv" aria-label="Search" name="query">';

    echo '</form>';

    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0 mt-4">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <br> <br>
        <?= $content ?>
    </div>
</main>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
