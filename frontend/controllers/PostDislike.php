<?php

namespace frontend\controllers;

use AllowDynamicProperties;
use yii\db\ActiveRecord;

class PostDislike extends ActiveRecord
{
    public static function tableName()
    {
        return 'post_dislike';
    }

    public function rules()
    {
        return [
            [['post_id', 'user_id'], 'required'],
            [['post_id', 'user_id'], 'integer'],
        ];
    }
}
