<?php

namespace frontend\controllers;

use AllowDynamicProperties;
use yii\db\ActiveRecord;

class PostLike extends ActiveRecord
{
    public static function tableName()
    {
        return 'post_like';
    }

    public function rules()
    {
        return [
            [['post_id', 'user_id'], 'required'],
            [['post_id', 'user_id'], 'integer'],
        ];
    }
}
