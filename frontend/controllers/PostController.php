<?php

namespace frontend\controllers;


use common\models\Post;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PostController extends Controller
{

    public function actionIncrementView($id): array
    {
        $post = Post::findOne($id);
        if ($post) {
            $post->views++;
            $post->save();
            return ['success' => true, 'views' => $post->views];
        } else {
            return ['success' => false];
        }
    }

    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
