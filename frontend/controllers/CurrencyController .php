<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;

class CurrencyController extends Controller
{
    public function actionIndex()
    {
        $rates = $this->getExchangeRates();
        if (empty($rates)) {
            return $this->render('index', ['rates' => [], 'error' => 'No exchange rates found.']);
        }
        return $this->render('index', ['rates' => $rates]);
    }




}
